// SPDX-License-Identifier: MIT

package main

import "github.com/SlyMarbo/rss"
import "fmt"
import "flag"

var feed_url = flag.String("u", "", "URL of RSS feed to tail")
var item_num = flag.Int("n", 0, "number of items to show")
var no_heading = flag.Bool("N", false, "do not show headings")
var one_shot = flag.Bool("1", false, "one shot")

func main() {
	flag.Parse()

	if *feed_url == "" {
		fmt.Println("Please give the URL of the RSS feed to check with the '-u' parameter.")
		return
	}

	feed, err := rss.Fetch(*feed_url)

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	rss_items := feed.Items
	if *item_num > 0 && *item_num < len(feed.Items) {
		rss_items = feed.Items[:*item_num]
	}

	show_heading := !(*no_heading)
	heading := ""
	if show_heading {
		heading = "Title:"
	}

	for _, item := range rss_items {
		fmt.Println(heading, item.Title)
	}

	// fmt.Println(feed)
}
